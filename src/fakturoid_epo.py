import collections
import datetime
from decimal import Decimal
import os
import xml.etree.ElementTree as ET

from appdirs import AppDirs
from tabulate import tabulate

DOWNLOADS_FOLDER = os.path.expanduser('~/Downloads')
APP_DIRS = AppDirs('fakturoid-epo', 'crabhi')

KH_PATH = APP_DIRS.user_data_dir + '/DPHKH_vzor.xml'
DP_PATH = APP_DIRS.user_data_dir + '/DPHDP3_vzor.xml'

Invoice = collections.namedtuple('Invoice', (
    'number',
    'client_country',
    'client_vat_no',
    'taxable_fulfillment_due',
    'paid_date',
    'base_21_amount',
    'vat_21_amount',
    'base_15_amount',
    'vat_15_amount',
))


def get_date(el, child):
    return datetime.datetime.strptime(el.findtext(child), '%Y-%m-%d').date()


def parse_invoices(filename):
    tree = ET.parse(filename)
    root = tree.getroot()
    invoices = []

    for invoice_el in root.findall('invoice'):
        invoice_number = invoice_el.findtext("number")

        base_amount = Decimal(invoice_el.findtext('subtotal'))
        line_amounts = Decimal(0)
        base_21_amount = Decimal(0)
        vat_21_amount = Decimal(0)
        base_15_amount = Decimal(0)
        vat_15_amount = Decimal(0)

        for line_el in invoice_el.find('lines').findall('line'):
            quantity = Decimal(line_el.findtext('quantity'))
            line_amount = quantity * Decimal(line_el.findtext('unit_price'))
            line_amounts += line_amount

            vat_rate = line_el.findtext('vat_rate')
            unit_price_with_vat = Decimal(line_el.findtext('unit_price_with_vat'))
            unit_price_without_vat = Decimal(line_el.findtext('unit_price_without_vat'))
            line_vat_amount = (unit_price_with_vat - unit_price_without_vat) * quantity

            if vat_rate == '15':
                vat_15_amount += line_vat_amount
                base_15_amount += unit_price_without_vat * quantity
            elif vat_rate == '21':
                vat_21_amount += line_vat_amount
                base_21_amount += unit_price_without_vat * quantity
            else:
                raise ValueError(f'Unknown VAT rate {vat_rate} ({invoice_number})')

        if base_amount != line_amounts:
            raise ValueError(f'Amount mismatch ({invoice_number})')

        client_country = invoice_el.findtext('client_country')
        client_vat_no = invoice_el.findtext('client_vat_no')
        if client_vat_no.startswith(client_country):
            client_vat_no = client_vat_no.lstrip(client_country)
        else:
            raise ValueError(f'Expected client vat No. to start with country code ({invoice_number})')


        invoices.append(Invoice(
            number=invoice_number,
            client_country=client_country,
            client_vat_no=client_vat_no,
            taxable_fulfillment_due=get_date(invoice_el, 'taxable_fulfillment_due'),
            paid_date=get_date(invoice_el, 'paid_on'),
            vat_15_amount=vat_15_amount,
            vat_21_amount=vat_21_amount,
            base_21_amount=base_21_amount,
            base_15_amount=base_15_amount,
        ))

    return invoices


def fill_kontrolni_hlaseni(invoices, year, month):
    doc = ET.parse(KH_PATH)
    kh_el, = doc.getroot().findall('DPHKH1')

    for line in kh_el.findall('VetaA4'):
        kh_el.remove(line)

    kh_el.remove(kh_el.find('VetaC'))

    header = kh_el.find('VetaD')
    header.set('mesic', str(month))
    header.set('rok', str(year))
    header.set('d_poddp', datetime.date.today().strftime('%d.%m.%Y'))

    low_invoices_base_21 = Decimal(0)
    low_invoices_vat_21 = Decimal(0)
    low_invoices_base_15 = Decimal(0)
    low_invoices_vat_15 = Decimal(0)

    for i, invoice in enumerate(invoices):
        if (invoice.base_21_amount + invoice.base_15_amount + invoice.vat_21_amount + invoice.vat_15_amount) > 10000:
            ET.SubElement(kh_el, 'VetaA4', {
                'c_radku': str(i + 1),
                'zakl_dane1': f'{invoice.base_21_amount:.0f}',
                'zakl_dane2': f'{invoice.base_15_amount:.0f}',
                'dan1': f'{invoice.vat_21_amount:.0f}',
                'dan2': f'{invoice.vat_15_amount:.0f}',
                'dic_odb': f'{invoice.client_vat_no}',
                'c_evid_dd': f'{invoice.number}',
                'dppd': invoice.taxable_fulfillment_due.strftime('%d.%m.%Y'),
                'zdph_44': 'N',  # Opravy u nedobytné pohledávky
                # Kód režimu plnění
                # Specifikace kódu režimu plnění:
                #
                #     0 - běžné plnění
                #     1 - § 89 ZDPH (zvláštní režim pro cestovní službu)
                #     2 - § 90 ZDPH (zvláštní režim pro použité zboží)
                'kod_rezim_pl': '0',
            })
        else:
            low_invoices_base_21 += invoice.base_21_amount
            low_invoices_base_15 += invoice.base_15_amount
            low_invoices_vat_21 += invoice.vat_21_amount
            low_invoices_vat_15 += invoice.vat_15_amount

    ET.SubElement(kh_el, 'VetaA5', {
        'zakl_dane1': f'{low_invoices_base_21:.0f}',
        'dan1': f'{low_invoices_vat_21:.0f}',
        'zakl_dane2': f'{low_invoices_base_15:.0f}',
        'dan2': f'{low_invoices_vat_15:.0f}',
    })

    ET.SubElement(kh_el, 'VetaC', {
        'obrat23': '{:.0f}'.format(sum(i.base_21_amount for i in invoices)),
    })

    if sum(i.base_15_amount for i in invoices) > 0:
        raise ValueError('Don\'t know how to write the sum into VetaC.')

    filename = DOWNLOADS_FOLDER + f'/KH_{year}_{month}.xml'
    doc.write(filename, encoding='utf-8', xml_declaration=True)
    print(f'Podání uloženo v {filename}')


def fill_danove_priznani_DPH(invoices, year, month):
    doc = ET.parse(DP_PATH)
    kh_el, = doc.getroot().findall('DPHDP3')

    header = kh_el.find('VetaD')
    header.set('mesic', str(month))
    header.set('rok', str(year))
    header.set('d_poddp', datetime.date.today().strftime('%d.%m.%Y'))

    revenue_21 = sum(i.base_21_amount for i in invoices)
    vat_21 = sum(i.vat_21_amount for i in invoices)
    vat_15 = sum(i.vat_15_amount for i in invoices)

    veta1 = kh_el.find('Veta1')
    veta1.set('obrat23', f'{revenue_21:.0f}')
    veta1.set('dan23', f'{vat_21:.0f}')

    if sum(i.base_15_amount for i in invoices) > 0:
        raise ValueError('Don\'t know how to write the sum into Veta1.')

    veta6 = kh_el.find('Veta6')
    veta6.set('dan_zocelk', f'{vat_21 + vat_15:.0f}')
    veta6.set('dano_da', f'{vat_21 + vat_15:.0f}')

    filename = DOWNLOADS_FOLDER + f'/DPHDP3_{year}_{month}.xml'
    doc.write(filename, encoding='utf-8', xml_declaration=True)
    print(f'Podání uloženo v {filename}')


def main():
    last_month = datetime.date.today().replace(day=1) - datetime.timedelta(days=1)

    print(f'Podání za {last_month:%Y/%m}')

    invoices = parse_invoices(f'{DOWNLOADS_FOLDER}/invoices_{last_month:%Y}_{last_month.month}m.xml')

    print(tabulate(invoices, headers=Invoice._fields, tablefmt='grid'))

    for invoice in invoices:
        if invoice.taxable_fulfillment_due.replace(day=1) != last_month.replace(day=1):
            raise ValueError(f'Wrong taxable date {invoice.taxable_fulfillment_due} ({invoice.number})')
        if not invoice.paid_date:
            print(f'Warning: Unpaid invoice: {invoice.number}')
        if invoice.client_country != 'CZ':
            raise ValueError('I don\'t know how to process foreign countries\' invoices')

    fill_kontrolni_hlaseni(invoices, last_month.year, last_month.month)
    fill_danove_priznani_DPH(invoices, last_month.year, last_month.month)


if __name__ == "__main__":
    main()
